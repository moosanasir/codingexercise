import json

def lambda_handler(event, context):
    # TODO implement
    return {
        'statusCode': 200,
        'body': json.dumps("""**Response**  """ +
        """ Welcome to our demo API, here are the details of your request:   """ +  
        """***Headers***   """+
        """Content-Type: """ + event['headers']['Content-Type'] +
        """  ***Method***   """ + event['httpMethod'] +
        """ Body: """ + event['body'] )
    }
