resource "aws_apigatewayv2_api" "coding-apigateway" {
  name          = "resource-coding-exercise-apigateway"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_stage" "coding-stage" {
  api_id      = aws_apigatewayv2_api.coding-apigateway.id
  name        = "resource-coding-excercise-apigateway-stage"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.coding-apigateway-log-group.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }

}

resource "aws_apigatewayv2_integration" "coding-get-integration" {
  api_id             = aws_apigatewayv2_api.coding-apigateway.id
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
  integration_uri    = aws_lambda_function.coding-lambda.invoke_arn
}

resource "aws_apigatewayv2_route" "coding-get-route" {
  api_id    = aws_apigatewayv2_api.coding-apigateway.id
  route_key = "GET /api"
  target    = "integrations/${aws_apigatewayv2_integration.coding-get-integration.id}"
}

resource "aws_apigatewayv2_integration" "coding-post-integration" {
  api_id             = aws_apigatewayv2_api.coding-apigateway.id
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
  integration_uri    = aws_lambda_function.coding-lambda.invoke_arn
}

resource "aws_apigatewayv2_route" "coding-post-route" {
  api_id    = aws_apigatewayv2_api.coding-apigateway.id
  route_key = "POST /api"
  target    = "integrations/${aws_apigatewayv2_integration.coding-post-integration.id}"
}

resource "aws_cloudwatch_log_group" "coding-apigateway-log-group" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.coding-apigateway.name}"

}
