output "lambda_url" {
  value = aws_lambda_function_url.coding-lambda-url.function_url
}

output "api_gateway_invoke_url" {
  value = aws_apigatewayv2_stage.coding-stage.invoke_url

}
