data "archive_file" "lambda-zip" {
  type        = "zip"
  source_file = "./function/function.py"
  output_path = "function.zip"
}

resource "aws_lambda_function" "coding-lambda" {
  # If the file is not in the current working directory you will need to include a
  # path.module in the filename.
  filename      = "function.zip"
  description   = "simple web application"
  function_name = "resource-coding-exercise-lambda"
  role          = aws_iam_role.lambda-role.arn
  handler       = "function.lambda_handler"

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  source_code_hash = data.archive_file.lambda-zip.output_base64sha256

  runtime = "python3.9"

}

resource "aws_lambda_permission" "coding-api-permissions" {
  statement_id  = "AllowCodingAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.coding-lambda.arn
  principal     = "apigateway.amazonaws.com"

  # The /*/*/* part allows invocation from any stage, method and resource path
  # within API Gateway REST API.
  source_arn = "${aws_apigatewayv2_api.coding-apigateway.execution_arn}/*/*/*"
}

resource "aws_lambda_function_url" "coding-lambda-url" {
  function_name      = aws_lambda_function.coding-lambda.arn
  authorization_type = "NONE"
}

resource "aws_cloudwatch_log_group" "function_log_group" {
  name = "/aws/lambda/${aws_lambda_function.coding-lambda.function_name}"
}
