data "archive_file" "lambda-zip" {
  type        = "zip"
  source_file = "./function/function.py"
  output_path = "function.zip"
}

module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "coding-exercise-lambda"
  description   = "simple web application"
  handler       = "function.lambda_handler"
  runtime       = "python3.9"
  publish       = true

  local_existing_package     = "function.zip"
  create_package             = false
  create_lambda_function_url = true

  tags = {
    Name = "my-lambda1"
  }
}
