output "lambda_url" {
  value = module.lambda_function.lambda_function_url
}

output "api_gateway_invoke_url" {
  value = module.api_gateway.default_apigatewayv2_stage_invoke_url
}
