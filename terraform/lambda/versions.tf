terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
    backend "s3" {
    bucket               = "coding-challenge-moosa"
    key                  = "coding-exercise-module"
    region               = "eu-central-1"
  }

  required_version = "~> 1.3"
}
