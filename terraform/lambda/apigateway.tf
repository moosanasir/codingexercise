module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "module-coding-exercise-apigateway"
  description   = "Api-Gateway for simple web-application"
  protocol_type = "HTTP"

  create_api_domain_name = false


  # Access logs
  default_stage_access_log_destination_arn = aws_cloudwatch_log_group.api-log-group.arn
  default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  # Routes and integrations
  integrations = {
    "POST /api" = {
      lambda_arn             = module.lambda_function.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }

    "GET /api" = {
      lambda_arn             = module.lambda_function.lambda_function_arn
      payload_format_version = "2.0"
      timeout_milliseconds   = 12000
    }
  }

  tags = {
    Name = "http-apigateway"
  }
}

resource "aws_cloudwatch_log_group" "api-log-group" {
  name = "coding-exercise-api-log-group"
}